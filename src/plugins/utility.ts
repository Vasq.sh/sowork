import Vue from "vue";

const Utility = {
	install(Vue: any, options: object) {
		Vue.prototype.$appName = "Sowork";

		Vue.prototype.$helloWorld = (): void => {
			alert("Hello");
		};

		Vue.prototype.$accessByString = (object: any, accessKey: string) => {
			accessKey = accessKey.replace(/\[(\w+)\]/g, ".$1"); // convert indexes to properties
			accessKey = accessKey.replace(/^\./, ""); // strip a leading dot
			let arrayOfResults: string[] = accessKey.split(".");
			for (var i = 0, n = arrayOfResults.length; i < n; ++i) {
				var coincidence = arrayOfResults[i];
				if (coincidence in object) {
					object = object[coincidence];
				} else {
					return;
				}
			}
			return object;
		};

		// 2. add a global asset
		Vue.directive("my-directive", {
			bind(el: any, binding: any, vnode: any, oldVnode: any) {
				// some logic ...
			},
		});

		// 3. inject some component options
		Vue.mixin({
			created: function() {
				// some logic ...
			},
		});

		// 4. add an instance method
		Vue.prototype.$myMethod = function(methodOptions: any) {
			// some logic ...
		};
	},
};

Vue.use(Utility);
