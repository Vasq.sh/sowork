import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import "./plugins/utility";
import VueI18n from "vue-i18n";
import messages from "./locale/locales";
Vue.use(VueI18n);
Vue.config.productionTip = false;

const i18n = new VueI18n({
	locale: "es", // set locale
	messages,
});

new Vue({
	router,
	store,
	i18n,
	render: h => h(App),
}).$mount("#app");
