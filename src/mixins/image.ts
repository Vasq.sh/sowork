const imageMixin = {
	methods: {
		fetchRandomImage(height: number, width: number, imageId?: number): string {
			return (
				// 'https://picsum.photos/' + height + '/' + width + '/?random'
				"https://unsplash.it/" + height + "/" + width + "?image=" + imageId
			);
		},
	},
};
export default imageMixin;
