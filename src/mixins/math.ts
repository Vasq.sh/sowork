const mathMixin = {
	methods: {
		getRandomNumber(): number {
			return Math.floor(Math.random() * 100) + 1;
		},
	},
};
export default mathMixin;
