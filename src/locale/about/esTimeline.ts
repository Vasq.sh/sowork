export default [
	{
		title: "Desarrollador web en Workflow EDV",
		date: "Ahora",
		description: '"mvn install -Dskiptests=True".',
	},
	{
		title: "Graduacion de la Universidad Privada del Valle",
		date: "Junio, 2018",
		description:
			'"Después de pasar 4 años de sentir que no sabes nada, que lo sabes todo, que no te enseñan nada y que te enseñan muy poco, llega la graduacion y&nbsp;...listo! aun sientes que no sabes nada".',
	},
	{
		title:
			"Mencion Honorable, ACM – ICPC South America/South Regional Contest.",
		date: "2017",
		description:
			'"La competición fue interesante, no ganamos pero aprendimos mucho".',
	},
	{
		title: "Escuela de la Sociedad Japonesa La Paz",
		date: "2015 - 2016",
		description: '"私は日本語を忘れました :(".',
	},
	{
		title: "Ingreso a la Universidad Privada del Valle",
		date: "2014",
		description: '"You are not prepared!". -Illidan Tempestira',
	},
	{
		title: "Graduacion del Colegio Santísima Trinidad",
		date: "2011 - Diciembre, 2013",
		description:
			'"Cuando es uno de los pocos dias que necesitas ser formal, y te lanzan el birrete por accidente justo antes de la foto ¯\\_(ツ)_/¯".',
	},
	{
		title: "Centro Boliviano Americano (CBA), English",
		date: "2013",
		description: '"Still better than premilitary service :D".',
	},
	{
		title: "Ingreso al Colegio Santísima Trinidad",
		date: "2011 - Diciembre, 2013",
		description:
			'"Aun no puedo creer que despues de un trabjo de refuerzo en computación, me decidiria por una carrera en la Universidad. (Era el mejor juego desarrollado en C++!)".',
	},
	{
		title: "Ingreso al Colegio María Inmaculada",
		date: "2002",
		description:
			'"Es algo extraño el tener a tu madre como profesora, todos piensan que es genial, hasta que te suspenden".',
	},
];
