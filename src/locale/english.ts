export default {
	hello: "Hello",
	welcome: "Welcome to {value}!",
	main: {
		description: "A small corner in the web.",
	},
	static: {
		motto: '~"Just keep going!"',
		aboutMe: "About me",
	},
};
