import timeline from "./about/esTimeline";

export default {
	hello: "Hola",
	welcome: "Bienvenido a {value}!",
	main: {
		description: "Un pequeño rincon en la red.",
	},
	about: {
		welcomeTitle: "¡Hola!, ¡Mi nombre es Adrian!",
		aboutMyself:
			"Soy un chico de 23 años, que vivió la mayor parte de su vida en La Paz, Bolivia.<br> Actualmente estoy haciendo una pasantía en Viena, Austria. A mas de 15 horas de viaje de mi familia.<br> Siendo hijo de una gran artista, siento una gran pasion por aprender diversos temas, tales como historia, politica, ciencias y especialmente arte, aunque no me consideraria un experto :) <br> Mi mayor afición son los videojuegos, aunque tambien me gusta la pintura y leer. O cualquier actividad tranquila y dentro de casa. <br> Aun asi, me gusta ir de vez en cuando por un viaje corto a algun lado y explorar nuevos lugares.",
		timeline: timeline,
	},
	static: {
		motto: '~"Solo sigue adelante!"',
		aboutMe: "Sobre mi",
	},
};
