import Español from "./español";
import English from "./english";
export default { es: Español, en: English };
