import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
interface Home {
	mainBannerImage: string;
}
interface About {
	mainBannerImage: string;
	imageList: string[];
}
const store = new Vuex.Store({
	state: {
		home: {
			mainBannerImage: "",
		},
		about: {
			mainBannerImage: "",
			imageList: [""],
		},
	},
	mutations: {
		setHomeState(state, data: Home) {
			state.home = data;
		},
		setAboutState(state, data: About) {
			state.about = data;
		},
	},
	actions: {},
	getters: {
		getHomeState(state): Home {
			return state.home;
		},
		getAboutState(state): About {
			return state.about;
		},
	},
});

export default store;
