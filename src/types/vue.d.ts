import Vue from "vue";
export type PluginFunction<T> = (_Vue: typeof Vue, options?: T) => void;

export interface PluginObject<T> {
	install: PluginFunction<T>;
	[key: string]: any;
}

declare module "vue/types/vue" {
	// 3. Declare augmentation for Vue
	interface Vue {
		$appName: string;
		$helloWorld(): void;
		fetchRandomImage(height: number, width: number, imageId?: number): string;
		getRandomNumber(): number;
		$accessByString(object: any, accessKey: string): any;
	}
}
