module.exports = {
	env: {
		browser: true,
		es6: true,
		node: true,
	},
	extends: [
		"plugin:vue/recommended",
		"eslint:recommended",
		"prettier/vue",
		"plugin:prettier/recommended",
	],
	globals: {
		Atomics: "readonly",
		SharedArrayBuffer: "readonly",
	},
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: "module",
		parser: "babel-eslint",
	},
	plugins: ["vue", "prettier"],
	rules: {
		"vue/component-name-in-template-casing": ["error", "PascalCase"],
		"no-console": process.env.NODE_ENV === "production" ? "error" : "off",
		"no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
	},
};
